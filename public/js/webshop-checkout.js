jQuery(function(){

	var cartItemHandlebarsTemplate = Handlebars.compile($("#cart-item-tmpl").html());

	var updateTotal = function() {
		var $totalEl = $('[data-cart="total"]');
		var total = 0;
		var data = localStorage_getObject('cart');
		$.each(data, function(index, item) {
			total = total + (new Number(item.price) * new Number(item.qty) );
		});
		$totalEl.text(total+'€');
	};

	var populateCart = function() {
		var data = localStorage_getObject('cart');
		$.each(data, function(index, item) {
			$('#cart-items').append( cartItemHandlebarsTemplate(item) );
		});
	};

	$(document).delegate('[data-action="remove"]', 'click', function(ev) {
		var cartItem = $(this).parents('[data-cart-item]');
		var id = cartItem.attr('data-cart-item');
		var storage = localStorage_getObject('cart');
		var cleanedStorage = $.map(storage, function(currentItem, index){
			if (currentItem.id != id) {
				return currentItem;
			}
			else {
				return null;
			}
		});

		localStorage_setObject('cart', cleanedStorage);
		cartItem.remove();
		updateCartCount(); //inherited from webshop-cart.js
		updateTotal();
	});

	$(document).delegate('[data-cart="qty"]', 'change', function(ev) {
		var $qtyEl = $(this);
		var cartItem = $(this).parents('[data-cart-item]');
		var id = cartItem.attr('data-cart-item');
		var storage = localStorage_getObject('cart');
		var updatedStorage = $.map(storage, function(currentItem, index) {
			if (currentItem.id == id) {
				currentItem.qty = $qtyEl.val();
				return currentItem;
			}
			else {
				return currentItem
			}
		});
		localStorage_setObject('cart', updatedStorage);
		updateTotal();
	});

	populateCart();
	updateTotal();

	//form validation

	var validateFields = function() {
		var isValid = true;
		$('[data-validation-rule]', $form).each(function(index, item) {
			var item = $(item);
			var rule = item.attr('data-validation-rule');
			var value = $.trim(item.val());

			//simple mandatory
			if(rule=="*"){
					var valid = value.length>0;
					isValid = isValid && valid;
					applyValidationStatus(item, valid, (valid ? undefined : 'Required'));
			}

			//regexp
			if(rule.substring(0,1) == '/') {
				rule = rule.substring(1);
				if (rule[rule.length-1] == '/') rule = rule.substring(0, rule.length-1);
				var regexp = new RegExp(rule);
				var valid = regexp.test(value);
				isValid = isValid && valid;
				applyValidationStatus(item, valid, (valid ? undefined : 'Invalid info, check') );
			}

			//equals another field
			if(rule.substring(0,1) == '#') {
				if (value.length==0) {
					var valid = false;
					isValid = false;
					applyValidationStatus(item, valid);
				}
				else {
					var masterValue = $(rule).val();
					var valid = (value == masterValue);
					isValid = isValid && valid;
					applyValidationStatus(item, valid, (valid ? undefined : 'Invalid info, check'));
				}
			}

		});
		return isValid;
	};

	var applyValidationStatus = function(input, isValid, message) {
		var $formgroup = $(input).parents('.form-group');
		$('span.help-block', $formgroup).remove();
		if(isValid) {
			$formgroup.addClass( 'has-success' );
			$formgroup.removeClass( 'has-error' );
		}
		else {
			$formgroup.removeClass( 'has-success' );
			$formgroup.addClass( 'has-error' );
		}
		if (message) {
			$formgroup.append('<span class="help-block">'+message+'</span>');
		}
	};

	var $form = $('#checkout-form');
	$form.on('submit', function(ev) {
		var isValid = validateFields();
		if(isValid) {
			//
		}
		else {
			ev.preventDefault();
			//alert('we');
		}
	});

	$form.delegate('input', 'input', validateFields);
	$form.delegate('input', 'change', validateFields);

});
