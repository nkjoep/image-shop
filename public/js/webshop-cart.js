//helpers
	localStorage_setObject = function(key, value) {
		var string = '';
		if(key) {
			string = JSON.stringify(value);
			localStorage.setItem(key, string);
		}
		return string
	};

	localStorage_getObject = function(key) {
		if(key) {
			try {
				return JSON.parse(localStorage.getItem(key));
			}
			catch(e) {
				return null;
			}
		}
	};

jQuery(function(){
	if(typeof(Storage) !== "undefined") {

		//init data
			var prevData = localStorage_getObject('cart');
			if (!prevData) {
				localStorage_setObject('cart', []);
			}

		var addToCart = function(obj) {
			var storage = localStorage_getObject('cart');

			storage = $.map(storage, function(storageItem, index) {
				if(storageItem.id!=obj.id) {
					return storageItem
				}
				else {
					return null
				}
			});
			storage.push(obj);
			localStorage_setObject('cart', storage);
			//console.log('live storage', localStorage_getObject('cart'));
			updateCartCount();
		};

		window.updateCartCount = function(items) {
			if(typeof items == 'number') {
				$('[data-cart-items-count]').text(items);
			}
			else {
				$('[data-cart-items-count]').text(localStorage_getObject('cart').length);
			}
		};

		$(document).delegate('[data-action="add-to-cart"]', 'submit', function(ev) {
			ev.preventDefault();
			$form = $(this);
			console.log('form', $form.serializeObject());
			var $btn = $('[data-loading-text]', $form).button('loading');
			addToCart( $(this).serializeObject() );
			window.setTimeout(function(){ //UX add a little delay it will be more human friendly...
				$btn.button('reset');
			}, 250);
		});

		//init
			updateCartCount();
	  	$('[data-toggle="tooltip"]').tooltip();
	}
	else {
			alert('We\'re sorry but your browser cannot support this online shop. Update it.');
	}

});
