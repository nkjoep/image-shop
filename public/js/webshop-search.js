var imageLoading = function(img) {
	img.style.opacity= '1';
};

jQuery(function(){ //domready

	//store our mediawiki connector
		window.mwjs = MediaWikiJS({baseURL: 'https://commons.wikimedia.org/', apiPath: '/w/api.php'});

	//for later use
		var $searchResultsEl = $('#results');
		var $loadMoreEl = $('.results-load-more-btn')
		var $loadMoreContainerEl = $('#results-load-more')
		var $inputQueryEl = $('.search-query');
		var $modal = $('#preview-modal');

	var doSearch = function(query, emptyResults) {
		if(query) {
			applyLoading();

			var queryObj = {
				action: 'query',
				list: 'allimages',
				format: 'json',
				aisort: 'name',
				ailimit: 6,
				aiprop: 'timestamp|user|userid|comment|parsedcomment|canonicaltitle|url|size|dimensions|sha1|mime|mediatype|metadata|commonmetadata|extmetadata|bitdepth|iiurlwidth=200',
				iiurlwidth: '200',
				aiurlwidth: '200',
				aifrom: query
			};

			mwjs.send(queryObj, function(data) {
				console.log('response all image', data);
				applyLoading();
				if (data.error) {
					renderError(data.error);
				}
				if (data.query) {
					var imageList = data.query.allimages;
					renderSearch(imageList, emptyResults);
				}
				if (data['query-continue']) {
					queryObj['query-continue'] = data['query-continue'].allimages;
					renderLoadMore(queryObj);
				}
			});
		}
		//else empty value?
	};


	var renderSearch = function(imageList, emptyResults) {
		console.log('response imageList', imageList);
		//removeLoading();
		if (emptyResults !== false) $searchResultsEl.empty();
		$.each(imageList, function(index, imageDataItem){

			//inserting a random price
				imageDataItem.price = Math.floor((Math.random() * 200) + 20);
			//inserting a random id
				imageDataItem.id = Math.floor((Math.random() * 999999) + 111111);

			setTimeout(function(){
				if(index>0 && index%3==0) {
					$searchResultsEl.append( '<div class="clearfix hidden-xs hidden-sm visible-md-block visible-lg-block"></div>' );
				}
				if(index>0 && index%2==0) {
					$searchResultsEl.append( '<div class="clearfix hidden-xs visible-sm-block hidden-md hidden-lg"></div>' );
				}
				var fragment = $(itemHandlebarsTemplate(imageDataItem));
				$('figure', fragment).data('imageData', imageDataItem);
				$searchResultsEl.append(fragment);
			}, (index*20));
		})
	};

	var renderLoadMore = function(continueValue) {
		$loadMoreContainerEl.removeClass('hidden');
		$loadMoreEl.removeClass('hidden');
		$loadMoreEl.off('click');
		$loadMoreEl.on('click', continueValue, function(ev){
				ev.preventDefault();
				doSearch(ev.data['query-continue']['aicontinue'], false);
		});
	};

	var renderError = function(errorObj) {
		$searchResultsEl.empty();
		console.log('error', errorObj);
		$searchResultsEl.append( errorHandlebarsTemplate(errorObj) );
	};


	var applyLoading = function() {
		var loading = $searchResultsEl.data('loading');
		if (loading) {
			$('[data-type="loading"]', $searchResultsEl).remove();
			$searchResultsEl.removeData('loading');
		}
		else {
			$searchResultsEl.data('loading', true);
			$searchResultsEl.append(loadingHandlebarsTemplate());
		}

	}



	//templates
		var itemHandlebarsTemplate = Handlebars.compile($("#results-item-tmpl").html());
		var errorHandlebarsTemplate = Handlebars.compile($("#error-query-tmpl").html());
		var loadingHandlebarsTemplate = Handlebars.compile($("#loading-query-tmpl").html());
		var modalbodyHandlebarsTemplate = Handlebars.compile($("#modal-body-tmpl").html());

		Handlebars.registerHelper("sizeKbytes", function(bytes) {
	  	return (bytes/1024).toFixed(2);
		});
		Handlebars.registerHelper("wikiToHtml", function(string) {
			return wiky.process(string);
		});
		Handlebars.registerHelper("unescape", unescape);
		Handlebars.registerHelper("JSONtoString", JSON.toString);
		Handlebars.registerHelper("sizeInMB", function(sizeInBytes) {
			return (new Number(sizeInBytes) / 1024 / 1024).toFixed(2);
		});

	//
	$inputQueryEl.on('keydown', function(ev) {
		$inputQueryEl.val($(this).val());
		if (ev.which == 13) {
			ev.preventDefault();
			doSearch( $inputQueryEl.val(), true );
		}
	});

	$('.search-query-button').on('click', function(ev) {
		ev.preventDefault();
		doSearch( $inputQueryEl.get(0).value, true );
	});

	$inputQueryEl.focus();

	//modal preview
	$searchResultsEl.delegate('figure', 'click', function(ev) {
		ev.preventDefault();
		var fragment = modalbodyHandlebarsTemplate( $(this).data('imageData') );
		$('.modal-dialog', $modal).html(fragment);
		//$('.modal-content', $modal).css('height',$( window ).height()*0.75);
		//$('.modal-content img', $modal).css('height',$( window ).height()*0.25);
		$modal.modal('show');
	});

	//just for starting a search from another page...
	if (window.location.search.length>0) {
		var query = window.location.search.substring(1).split('q=')[1].split('&')[0];
		if (query.length>0)
			doSearch(unescape(query), true);
			$inputQueryEl.val(query);
	}


}); //domready
