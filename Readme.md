# Image Shop

## How To Run It

* <code>$ npm install </code>
* <code>$ node ./bin/www</code>
* or if you want to debug <code>$ DEBUG=image-shop node ./bin/www</code>


## Notes

* After reading the requirements, the first step is to understand the MediaWiki API. I worked with those API in the past, but sincerely was for a personal project and or just for experiments. Found and studied the [MediaWiki API page](http://www.mediawiki.org/wiki/API:Main_page)

* Created a very basic nodejs webapp with the **express generator**:
```bash
$ npm install express-generator -g
$ express -c less --ejs image-shop
```

* Created the homepage adding the main functionality, the ajax search to wikimedia.org in order to retrieve some results. First problem here: I think mediawiki doesn't expose an api to search images. It's possible to search images but you'll get only original sizes. The **imageinfo** (**ii**) property is something missing within the **allimages** api.
Also, the **allimages** api doesn't return any id. Why do I need an **id**? In order to make the page of the details. So I decided to use fake info, I'm not very happy with but I suppose for the exercise will work. So at the moment the detail page is dummy.

* Cart: added the cart functionality using the localstorage, some strange things could happen when you don't have IDs of the items you want to list, however it's an easy check to do with the code.

* Cart Checkout: added a simple validation with messages. It's possibile to add a custom validation to each field using the attribute **data-validation-rule**: __*__ for simple mandatory fields, __/regxp/__ to validate with a regular expression, __#id__ to create a dependency on another field (think of the email written twice as confirmation).

* a live is running at: [https://image-shop.herokuapp.com/](https://image-shop.herokuapp.com/)



## Future Improvements

If this were a real project:

* Find a better api for the images, or write a server-side app for that
* Introduce a better responsiveness for the images: with more time the images **must** be improved in weight and transfer time
* The detail page has to be rethinked: with a proper api in order to generate unique URL for each item. Useful for the users so it's possible to bookmark the page, useful for the SEO. Within the detail page I'd add something useful: related items, hot /most purchased, purchased also...
* Checkout system: it needs a real validation system, and I mean a server side validation in order to prevent spam and trolls. Also a db where to store sessions and run a bit of BI to improve sales.
