var express = require('express');
var router = express.Router();
var request = require('request');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'WebShop' });
});

/* proxy */ //maybe if there's time
router.get('/media-wiky-proxy', function(req, res) {
  request('http://commons.wikimedia.org/').pipe(res);
});

module.exports = router;
