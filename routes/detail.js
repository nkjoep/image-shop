var express = require('express');
var router = express.Router();

router.param('id', function (req, res, next, id) {
  req.id = id;
  next();
})

router.get('/:id', function(req, res) {
	  res.render('detail', { title: 'WebShop / Detail', id: req.id });
});

module.exports = router;
