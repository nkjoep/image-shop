var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
	  res.render('checkout', { title: 'WebShop / Checkout', status: 'checkout' });
});

router.get('/thank-you', function(req, res) {
	  res.render('checkout', { title: 'WebShop / Checkout / Thank You', status: 'thankyou' });
});

module.exports = router;
